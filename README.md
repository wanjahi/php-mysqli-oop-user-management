this is a simple object oriented user management system (login, register and log out) written in php and uses mysql Database

create Database as follows
CREATE TABLE users
(
userId INT PRIMARY KEY AUTO_INCREMENT,
userName VARCHAR(30) UNIQUE,
userPass VARCHAR(50),
fullName VARCHAR(100),
userEmail VARCHAR(70) UNIQUE
);
